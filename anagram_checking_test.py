import unittest
from anagram_checking import AnagramChecker
from read_file import Read
from unittest import mock


w=Read("input.txt").readFromFile()

class beforemethod(unittest.TestCase):
    
    def setUp(self):
        self.word=AnagramChecker()
    
    def tearDown(self) :
        self.word=None
        
class TestAnagram(beforemethod):

    @mock.patch("read_file.Read")
    def test_mock(self,mock_b_constructor):
        mock_b=mock_b_constructor.return_value
        mock_b.readFromFile.return_value=1
        words=w[2].split(",")
        self.assertEqual(word.check(words[0].strip(),words[1].strip()),1)
    
    def test_empty_string(self):
        words = w[0].split(",")
        self.assertEqual(word.check(words[0].strip(),words[1].strip),0)

    
    def test_string(self):
        words = w[1].split(",")
        self.assertEquals(word.check(words[0].strip(),words[1].strip),1)
        
    def test_string(self):
        words = w[3].split(",")
        self.assertEquals(word.check(words[0].strip(),words[1].strip),1)    
    
    def test_not_present_string(self):
        words=w[4].split(",")
        self.assertEqual(word.check(words[0].strip(),words[1].strip),0)
        
        










